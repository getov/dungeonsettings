## Interface: 70300
## Author: Peter Getov
## Version: 1.0
## Title: |cfffe7b2cDungeonSettings|r
## Notes: Automated loot settings for raids
## DefaultState:  enabled
## SavedVariables: DungeonSettingsDB
## SavedVariablesPerCharacter: DungeonSettingsCharacterDB

DungeonSettingsUI.lua
DungeonSettingsCore.lua