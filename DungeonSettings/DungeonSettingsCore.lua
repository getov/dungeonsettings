-- Access the namespace
local _, core = ...;

local lootMethodsIDs = {
    "group",
    "personalloot",
    "freeforall",
    "master"
}

local partyEventFrame = CreateFrame("Frame");
partyEventFrame:RegisterEvent("PARTY_LEADER_CHANGED");
partyEventFrame:RegisterEvent("GROUP_JOINED");

partyEventFrame:SetScript("OnEvent",
    function(self, event, ...)
        local isLeader = UnitIsGroupLeader("player");

        if (isLeader) then
            -- +1 because we want it to go from 2 (Uncommon)
            SetLootThreshold(DungeonSettingsCharacterDB.selectedLootThreshold + 1);
        end
    end)

local dungeonEventFrame = CreateFrame("Frame");
dungeonEventFrame:RegisterEvent("ZONE_CHANGED_NEW_AREA");

dungeonEventFrame:SetScript("OnEvent",
    function(self, event, ...)
        -- name, instanceType, difficultyID, difficultyName, maxPlayers, dynamicDifficulty, isDynamic, instanceMapID, instanceGroupSize = GetInstanceInfo()
        local _, _, difficultyID, _, maxPlayers, _, _, _, _ = GetInstanceInfo();

        -- name, groupType, isHeroic, isChallengeMode, displayHeroic, displayMythic, toggleDifficultyID = GetDifficultyInfo(difficultyID)
        local _, groupType, _, _, _, _, _ = GetDifficultyInfo(difficultyID);

        if (groupType == "raid" or groupType == "party") then
            if (DungeonSettingsCharacterDB.showStopwatch) then
                StopwatchFrame:Show();
            else
                StopwatchFrame:Hide();
            end
        end

        if (groupType == "raid") then
            local isLeader = UnitIsGroupLeader("player");

            if (isLeader) then
                local method = lootMethodsIDs[DungeonSettingsDB.selectedLootMethod];
                SetLootMethod(method);
            end
        end
    end)