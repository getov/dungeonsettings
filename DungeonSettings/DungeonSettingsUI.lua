-- Namespaces
-- core - table (namespace) shared between every lua file
local addonName, core = ...;
core.UIConfig = {};

-- Defaults
local UISettingsGlobal = {
    selectedLootMethod = 1;
}

local UISettingsCharacter = {
    selectedLootThreshold = 1;
    showStopwatch = false;
}

-- for easier referencing the core config
local UIConfig = core.UIConfig;
local DS_ConfigFrame = {};

local lootMethodsDropDownList = {
    "Group Loot",
    "Personal Loot",
    "Free for all",
    "Master Looter"
}

local lootThresholdDropDownList = {
    "Uncommon",
    "Rare",
    "Epic",
    "Legendary"
}

local function UIElementShowTooltip(self)
    GameTooltip:SetOwner(self, "ANCHOR_RIGHT");
    GameTooltip:SetText(self.tooltipText, nil, nil, nil, nil, true);
    GameTooltip:Show();
end

local function UIElementHideTooltip(self)
    GameTooltip_Hide();
end

local function SetLootThresholdChecked()
    local isLeader = UnitIsGroupLeader("player");

    if (isLeader) then
        -- +1 because we want it to go from 2 (Uncommon)
        SetLootThreshold(DungeonSettingsCharacterDB.selectedLootThreshold + 1);
    end
end

-- TODO: combine these somehow?
local function OnClick(self)
    UISettingsGlobal.selectedLootMethod = self:GetID();
    UIDropDownMenu_SetSelectedID(DS_ConfigFrame.InterfaceOptionsPanel.lootMethodsDropDown, UISettingsGlobal.selectedLootMethod);
end

local function OnClickLootThreshold(self)
    UISettingsCharacter.selectedLootThreshold = self:GetID();
    UIDropDownMenu_SetSelectedID(DS_ConfigFrame.InterfaceOptionsPanel.lootThresholdDropDown, UISettingsCharacter.selectedLootThreshold);
    SetLootThresholdChecked();
end

function UIConfig:CreateMenu()
    DS_ConfigFrame.InterfaceOptionsPanel = CreateFrame("Frame", "DungeonSettingsInterfaceOptionsPanel", UIParent);
    DS_ConfigFrame.InterfaceOptionsPanel.name = "DungeonSettings";
    InterfaceOptions_AddCategory(DS_ConfigFrame.InterfaceOptionsPanel);

    DS_ConfigFrame.InterfaceOptionsPanel.title = CreateFrame("Frame", "DungeonSettings", DS_ConfigFrame.InterfaceOptionsPanel);
    DS_ConfigFrame.InterfaceOptionsPanel.title:SetPoint("TOPLEFT", DS_ConfigFrame.InterfaceOptionsPanel, "TOPLEFT", 10, -10);
    DS_ConfigFrame.InterfaceOptionsPanel.title:SetWidth(300);
    DS_ConfigFrame.InterfaceOptionsPanel.title:SetWidth(100);
    
    DS_ConfigFrame.InterfaceOptionsPanel.titleString = DS_ConfigFrame.InterfaceOptionsPanel.title:CreateFontString(nil, "OVERLAY", "GameFontNormal");
    DS_ConfigFrame.InterfaceOptionsPanel.titleString:SetPoint("TOPLEFT", DS_ConfigFrame.InterfaceOptionsPanel, "TOPLEFT", 10, -10);
    DS_ConfigFrame.InterfaceOptionsPanel.titleString:SetText('|cff00c0ffDungeonSettings|r');
    DS_ConfigFrame.InterfaceOptionsPanel.titleString:SetFont("Fonts\\FRIZQT__.tff", 20, "OUTLINE");

    -- Loot method dropdown and label
    local lootMethodLabel =  DS_ConfigFrame.InterfaceOptionsPanel:CreateFontString(nil, "OVERLAY", "GameFontNormal");
    lootMethodLabel:SetPoint("TOPLEFT", DS_ConfigFrame.InterfaceOptionsPanel, "TOPLEFT", 20, -40);
    lootMethodLabel:SetText('Loot Method');
    lootMethodLabel:SetFont("Fonts\\FRIZQT__.tff", 12, "OUTLINE");

    DS_ConfigFrame.InterfaceOptionsPanel.lootMethodsDropDown = CreateFrame("Frame", "DS_LootMethodsDropdown", DS_ConfigFrame.InterfaceOptionsPanel, "UIDropDownMenuTemplate");
    DS_ConfigFrame.InterfaceOptionsPanel.lootMethodsDropDown:SetPoint("TOPLEFT", DS_ConfigFrame.InterfaceOptionsPanel, "TOPLEFT", 0, -55);

    -- Tooltip
    DS_ConfigFrame.InterfaceOptionsPanel.lootMethodsDropDown.tooltipText = "Sets the default loot method.\nIt takes effect when you are the group leader.";
    DS_ConfigFrame.InterfaceOptionsPanel.lootMethodsDropDown:SetScript("OnEnter", UIElementShowTooltip);
    DS_ConfigFrame.InterfaceOptionsPanel.lootMethodsDropDown:SetScript("OnLeave", UIElementHideTooltip);
    -- End Loot method dropdown and label

    -- Loot threshold dropdown and label
    local lootThresholdLabel = DS_ConfigFrame.InterfaceOptionsPanel:CreateFontString(nil, "OVERLAY", "GameFontNormal");
    lootThresholdLabel:SetPoint("TOPLEFT", DS_ConfigFrame.InterfaceOptionsPanel, "TOPLEFT", 200, -40);
    lootThresholdLabel:SetText('Loot Threshold');
    lootThresholdLabel:SetFont("Fonts\\FRIZQT__.tff", 12, "OUTLINE");

    DS_ConfigFrame.InterfaceOptionsPanel.lootThresholdDropDown = CreateFrame("Frame", "DS_LootThresholdDropdown", DS_ConfigFrame.InterfaceOptionsPanel, "UIDropDownMenuTemplate");
    DS_ConfigFrame.InterfaceOptionsPanel.lootThresholdDropDown:SetPoint("TOPLEFT", DS_ConfigFrame.InterfaceOptionsPanel, "TOPLEFT", 180, -55);

    -- Tooltip
    DS_ConfigFrame.InterfaceOptionsPanel.lootThresholdDropDown.tooltipText = "Sets the default loot threshold.\nIt takes effect when you are the group leader.";
    DS_ConfigFrame.InterfaceOptionsPanel.lootThresholdDropDown:SetScript("OnEnter", UIElementShowTooltip);
    DS_ConfigFrame.InterfaceOptionsPanel.lootThresholdDropDown:SetScript("OnLeave", UIElementHideTooltip);
    -- End Loot threshold dropdown and label

    -- Check Buttons
    DS_ConfigFrame.InterfaceOptionsPanel.checkButtonStopwatch = CreateFrame("CheckButton", "default", DS_ConfigFrame.InterfaceOptionsPanel, "UICheckButtonTemplate");
    DS_ConfigFrame.InterfaceOptionsPanel.checkButtonStopwatch:SetPoint("TOPLEFT", 20, -100);
    DS_ConfigFrame.InterfaceOptionsPanel.checkButtonStopwatch.text:SetText("Show Stopwatch on entering an instance");
    DS_ConfigFrame.InterfaceOptionsPanel.checkButtonStopwatch:SetChecked(UISettingsCharacter.showStopwatch);
    DS_ConfigFrame.InterfaceOptionsPanel.checkButtonStopwatch:SetScript("OnClick", 
                                                                        function()
                                                                            UISettingsCharacter.showStopwatch = not UISettingsCharacter.showStopwatch;
                                                                        end);
end

-- TODO: make this universal
function UIConfig:DungeonSettingsUI_InitializeDropdown(self, level)
    local info = UIDropDownMenu_CreateInfo();
    for k,v in pairs(lootMethodsDropDownList) do
        info.text = v;
        info.func = OnClick;
        info.checked = false;
        UIDropDownMenu_AddButton(info, level);
     end
end

function UIConfig:DungeonSettingsUI_InitThresholdDropdown(self, level)
    local info = UIDropDownMenu_CreateInfo();
    for k,v in pairs(lootThresholdDropDownList) do
        info.text = v;
        info.func = OnClickLootThreshold;
        info.checked = false;
        UIDropDownMenu_AddButton(info, level);
     end
end

function UIConfig:SetupDropdown()
    UIConfig:CreateMenu();
    UIDropDownMenu_Initialize(DS_ConfigFrame.InterfaceOptionsPanel.lootMethodsDropDown, UIConfig.DungeonSettingsUI_InitializeDropdown);
    UIDropDownMenu_SetSelectedID(DS_ConfigFrame.InterfaceOptionsPanel.lootMethodsDropDown, UISettingsGlobal.selectedLootMethod);

    UIDropDownMenu_Initialize(DS_ConfigFrame.InterfaceOptionsPanel.lootThresholdDropDown, UIConfig.DungeonSettingsUI_InitThresholdDropdown);
    UIDropDownMenu_SetSelectedID(DS_ConfigFrame.InterfaceOptionsPanel.lootThresholdDropDown, UISettingsCharacter.selectedLootThreshold);
end

-- Serializing the DB
local dbLoader = CreateFrame("Frame");
dbLoader:RegisterEvent("ADDON_LOADED");
dbLoader:RegisterEvent("PLAYER_LOGOUT");

-- ADDON_LOADED is called after the code of the addon is being executed
-- Therefore I have to call any setup-functions dependent on the DB after the event (UIConfig:SetupDropdown())
function dbLoader:OnEvent(event, arg1)
    if (event == "ADDON_LOADED" and arg1 == "DungeonSettings") then
        -- Global DB
        if (DungeonSettingsDB == nil) then
            DungeonSettingsDB = UISettingsGlobal;
        else
            UISettingsGlobal = DungeonSettingsDB;
        end
        
        -- Character DB
        if (DungeonSettingsCharacterDB == nil) then
            DungeonSettingsCharacterDB = UISettingsCharacter;
        else
            UISettingsCharacter = DungeonSettingsCharacterDB;
        end

        UIConfig:SetupDropdown();
    elseif (event == "PLAYER_LOGOUT") then
        DungeonSettingsDB = UISettingsGlobal;
        DungeonSettingsCharacterDB = UISettingsCharacter;
    end
end

dbLoader:SetScript("OnEvent", dbLoader.OnEvent);